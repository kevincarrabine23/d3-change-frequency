/**
 * Application entry point
 */

// Load application styles
import 'styles/index.scss';

import { helpers } from './utils/helpers.js';

const labels = {
    xAxisTitle: 'Files sorted by change frequency',
    yAxisTitle: 'Change frequency'
}

const data = {
    angular: {
        path: '../assets/data/angular.csv',
        xAxisTitle: labels.xAxisTitle,
        yAxisTitle: labels.yAxisTitle,
        title: 'Change Frequncy Distribution for Angular 6.0'
    },
    react: {
        path: '../assets/data/react.csv',
        xAxisTitle: labels.xAxisTitle,
        yAxisTitle: labels.yAxisTitle,
        title: 'Change Frequncy Distribution for react 15-stable'
    }   
}

const graphRender = function(pathToData, xAxisTitle, yAxisTitle, chartTitle) {
    'use strict';

    const margin = {
        top: 50,
        right: 30,
        bottom: 80,
        left: 70
    };

    // Set the inner area of the graph. increasing the left and right margin will increase the width of the graph
    // increasing the top and bottom margin will increase the height of the graph
    const width = (1000 - margin.left - margin.right);
    const height = (500 - margin.top - margin.bottom);

    // Create axis scale
    const x = d3.scaleLinear().range([0, width]);
    const y = d3.scaleLinear().range([height, 0]);

    const tip = d3.tip()
        .attr('class', 'd3-tip')
        .offset([-10, 0])
        .html((d) => {
            return `
                        <strong>Frequency:</strong> <span style='color:red'>${d.changeFrequency}</span><br />
                        <strong>File:</strong> <span style='color:red'>${d.file}</span>
                    `;
        });

    // Create svg to draw graph
    const svg = d3.select('body').append('svg')
        .attr('width', width + margin.left + margin.right)
        .attr('height', height + margin.top + margin.bottom)
        .append('g')
        .attr('transform', `translate(${margin.left},${margin.top})`)

    svg.call(tip);

    // Grab the data
    d3.csv(pathToData).then((data) => {

        // Sort the data by changeFrequency as a sanity check
        data.sort((a, b) => {
            return b.changeFrequency - a.changeFrequency;
        });

        // Scale the range of the data
        x.domain([0, data.length]);

        y.domain([0, d3.max(data, (d) => {
            // Has to be of type int
            return parseInt(d.changeFrequency);
        }) + 50]);

        svg.selectAll('.bar')
            .data(data)
            .enter()
            .append('rect')
            .attr('class', 'bar')
            .attr('x', (d, i) => {
                return x(i)
            })
            .attr('width', '5')
            .attr('y', (d) => {
                return y(d.changeFrequency);
            })
            .attr('height', (d) => {
                return height - y(d.changeFrequency);
            })
            .on('mouseover', tip.show)
            .on('mouseout', tip.hide)

        // Add the X axis
        svg.append('g')
            .attr('transform', `translate(0, ${height})`)
            .call(d3.axisBottom(x).ticks(8));

        // Add the Y axis
        svg.append('g')
            .call(d3.axisLeft(y).ticks(5));

        // X axis text label
        helpers.setXaxisText({ svg, xAxisTitle, width, height, margin });

        // Y axis text label
        helpers.setYaxisText({ svg, yAxisTitle, height, margin });

        // Set chart title
        helpers.setChartTitle({ svg, chartTitle, width, margin });
    }).catch((e) => {
        console.error('Error fetching data:', e);
    });
}

Object.keys(data).forEach((f) => {
    let frameWork = data[f];
    graphRender(frameWork.path, frameWork.xAxisTitle, frameWork.yAxisTitle, frameWork.title);
});