export const helpers = {
    setXaxisText: (config) => {
        config.svg.append('text')
            .attr('transform', `translate(${config.width/2},${config.height + config.margin.top + 20})`)
            .style('text-anchor', 'middle')
            .text(config.xAxisTitle)
    },
    setYaxisText: (config) => {
        config.svg.append("text")
            .attr("transform", "rotate(-90)")
            .attr("y", 0 - config.margin.left)
            .attr("x", 0 - (config.height / 2))
            .attr("dy", "1em")
            .style("text-anchor", "middle")
            .text(config.yAxisTitle);
    },
    setChartTitle: (config) => {
        config.svg.append("text")
            .attr("x", (config.width / 2))
            .attr("y", 0 - (config.margin.top / 2))
            .attr("text-anchor", "middle")
            .style("font-size", "16px")
            .style("text-decoration", "underline")
            .text(config.chartTitle);
    }
}